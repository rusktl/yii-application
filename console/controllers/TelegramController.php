<?php

namespace console\controllers;

class TelegramController extends \yii\console\Controller
{
    public $message;

    public function options($actionID)
    {
        return ['message'];
    }

    public function optionAliases()
    {
        return ['m' => 'message'];
    }

    public function actionIndex()
    {
        \Yii::$app->telegram->sendMessage([
            'chat_id' =>  365493021,
            'text' => $this->message,
        ]);
    }
}